#!/usr/bin/env python
import glob
import os
import sys
import xml.etree.ElementTree

import cv2
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import tensorflow
from keras.preprocessing.image import ImageDataGenerator
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import LabelEncoder
from tensorflow.keras.applications import MobileNetV2
from tensorflow.keras.applications.mobilenet_v2 import preprocess_input
from tensorflow.keras.layers import Input
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.preprocessing.image import img_to_array
from tensorflow.keras.utils import to_categorical

DATA_PATH = ''
# MODEL_INPUT_RES = [96, 128, 160, 192, 224]
MODEL_INPUT_RES = 224
# MODEL_MIX_RES = 16
MODEL_MIX_RES = 24
EPOCHS = 10
BATCH_SIZE = 1


def save_model(model):
    save_dir = 'C:\\dev\\Python\\lab5\\results\\model\\'

    model_json = model.to_json()
    with open(save_dir + 'model.json', 'w') as f:
        f.write(model_json)

    model.save_weights(save_dir + 'model.h5')
    print('Saved model to', save_dir)


def check_files():
    image_count = annotation_count = 0
    for dirname, _, filenames in os.walk(DATA_PATH + '\\images'):
        image_count = len(filenames)
    for dirname, _, filenames in os.walk(DATA_PATH + '\\annotations'):
        annotation_count = len(filenames)
    if image_count != annotation_count:
        print('Count of images not equal to count of annotations')
    print(f'{image_count} images found')


def create_df():
    # init dictionary
    MAX_FACES = 50
    dic = {'Image name': [], 'Dimensions': []}
    for i in range(1, MAX_FACES + 1):
        dic[f'Object {i}'] = []

    for file in os.listdir(DATA_PATH + '\\annotations'):
        row = []
        xml_data = xml.etree.ElementTree.parse(DATA_PATH + '\\annotations\\' + file).getroot()

        # image name and dimensions
        row.append(xml_data[1].text)
        row.append([xml_data[2][0].text, xml_data[2][1].text])
        # objects
        for i in range(4, len(xml_data)):
            temp = []
            if xml_data[i][0].text == 'mask_weared_incorrect':
                temp.append('with_mask')
            else:
                temp.append(xml_data[i][0].text)
            for point in xml_data[i][5]:
                temp.append(point.text)
            row.append(temp)
        for i, each in enumerate(dic):
            dic[each].append(row[i] if i < len(row) else 0)
    df = pd.DataFrame(dic)
    return df


def get_X_y(df):
    image_directories = sorted(
        glob.glob(os.path.join(DATA_PATH + '\\images', '*.png')))
    classes = {'without_mask': 0, 'with_mask': 1, 'mask_weared_incorrect': 1}
    X = []
    y = []

    print('Image processing... ', end='')
    for idx, image in enumerate(image_directories):
        img = cv2.imread(image)
        width, height = df['Dimensions'][idx]
        cv2.resize(img, (int(width), int(height)))
        for obj in df.columns[3:]:
            if df[obj][idx] == 0:
                continue
            info = df[obj][idx]
            info[0] = classes[info[0]]
            info = [int(i) for i in info]
            face = img[info[2]:info[4], info[1]:info[3]]
            if (info[3] - info[1]) > MODEL_MIX_RES and (info[4] - info[2]) > MODEL_MIX_RES:
                try:
                    face = cv2.resize(face, (MODEL_INPUT_RES, MODEL_INPUT_RES))
                    face = img_to_array(face)
                    face = preprocess_input(face)
                    X.append(face)
                    y.append(info[0])
                except:
                    print(f'face conversion failed {image}')
    print(f'Done, {len(y)} faces found')
    X = np.array(X, dtype='float32')
    y = LabelEncoder().fit_transform(y)
    y = to_categorical(y)
    return X, y


def my_model(X, y):
    POOL_SIZE = 5 if MODEL_INPUT_RES >= 160 else 2
    INIT_LR = 1e-4
    X_train, X_test, y_train, y_test = train_test_split(
        X, y, test_size=0.25, stratify=y, random_state=42)

    # we will use pre-trained model because we don't have much training data
    baseModel = MobileNetV2(
        weights='imagenet',
        include_top=False,
        input_tensor=Input(shape=(MODEL_INPUT_RES, MODEL_INPUT_RES, 3))
    )
    baseModel.trainable = False

    # create our final model by adding more layers
    model = tensorflow.keras.Sequential([
        baseModel,
        tensorflow.keras.layers.AveragePooling2D(
            pool_size=(POOL_SIZE, POOL_SIZE)),
        tensorflow.keras.layers.Flatten(name='flatten'),
        tensorflow.keras.layers.Dense(64, activation='relu'),
        tensorflow.keras.layers.Dropout(0.5),
        tensorflow.keras.layers.Dense(2, activation='softmax')
    ])
    print(model.summary())

    print('Model compilation... ', end='')
    model.compile(
        loss='categorical_crossentropy',
        optimizer=Adam(lr=INIT_LR, decay=INIT_LR / EPOCHS),
        metrics=['accuracy']
    )
    print('Done')

    aug = ImageDataGenerator(
        zoom_range=0.1,
        rotation_range=25,
        width_shift_range=0.1,
        height_shift_range=0.1,
        shear_range=0.15,
        horizontal_flip=True,
        fill_mode='nearest'
    )

    history = model.fit(
        aug.flow(X_train, y_train, batch_size=BATCH_SIZE),
        validation_data=(X_test, y_test),
        steps_per_epoch=len(X_train) // BATCH_SIZE,
        validation_steps=len(X_test) // BATCH_SIZE,
        epochs=EPOCHS,
        batch_size=BATCH_SIZE,
        verbose=2,
        workers=8
    )
    title = f'Batch {BATCH_SIZE}, face count {len(y)}, ' \
            f'input res {MODEL_INPUT_RES}, face size {MODEL_MIX_RES}'
    plt.figure()
    plt.plot(range(EPOCHS),
             history.history['val_accuracy'], label='Validation Accuracy')
    plt.title(title)
    plt.xlabel('Epoch')
    plt.ylabel('Loss/Accuracy %')
    plt.legend(loc='lower left')
    plt.savefig(f'results\\{title}.png')
    plt.show()

    save_model(model)


if __name__ == '__main__':
    DATA_PATH = os.path.abspath(os.getcwd()) + '\\data'
    sys.stdout = open('stdout.txt', 'w')

    check_files()
    annotations_df = create_df()
    X, y = get_X_y(annotations_df)
    my_model(X, y)
    print('Done')
